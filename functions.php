<?php declare( strict_types=1 );

/* Enqueue parent theme styles */
add_action( 'wp_enqueue_scripts', function () {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
} );

/**
 * Adds a random image to user upon registration.
 *
 * @param int $user_id
 */
function tt_user_register( int $user_id ): void {

	$photo_id = rand( 1, 5000 );

	$curl = curl_init();
	curl_setopt_array( $curl, [
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL            => 'https://jsonplaceholder.typicode.com/photos/' . $photo_id,
	] );
	$response = curl_exec( $curl );
	curl_close( $curl );

	$data = json_decode( $response );

	update_user_meta( $user_id, 'register_image', $data->url );
}

add_action( 'user_register', 'tt_user_register', 10, 1 );

/**
 * Captures user agent at login.
 *
 * @param string $user_login
 * @param WP_User $user
 */
function tt_user_logged_in( string $user_login, WP_User $user ): void {
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	update_user_meta( $user->ID, 'last_user_agent', $user_agent );
}

add_action( 'wp_login', 'tt_user_logged_in', 10, 2 );

/**
 * Enqueues WP default jQuery and jQuery validate.
 */
function tt_enqueue_scripts(): void {

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script(
		'site-main',
		get_stylesheet_directory_uri() . '/main.js',
		[ 'jquery' ],
		filemtime( get_stylesheet_directory() . '/main.js' )
	);

	wp_localize_script( 'site-main', 'site_data', [
		'ajax_url' => admin_url( 'admin-ajax.php' ),
	] );
}

add_action( 'wp_enqueue_scripts', 'tt_enqueue_scripts' );

/**
 * Returns inspirational quote on AJAX call.
 *
 * Caches each quote for 30 minutes before retrieving another.
 */
function tt_get_quote(): void {

	if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) {
		return;
	}

	$qotd = get_transient( 'quote_of_the_day' );

	if ( ! $qotd ) {
		$curl = curl_init();
		curl_setopt_array( $curl, [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL            => 'https://api.kanye.rest/',
		] );
		$response = curl_exec( $curl );
		curl_close( $curl );

		$data = json_decode( $response );
		$qotd = $data->quote;

		$cache_timeout = apply_filters( 'hl/quote/cache/timeout', 0 );

		set_transient( 'quote_of_the_day', $qotd, $cache_timeout );
	}

	echo $qotd;
	exit;
}

add_action( 'wp_ajax_get_quote', 'tt_get_quote' );

/**
 * Returns a desired cache timeout.
 *
 * @param int $timeout
 *
 * @return int
 */
function hl_set_cache_timeout( int $timeout ): int {
	return MINUTE_IN_SECONDS * 30;
}

add_filter( 'hl/quote/cache/timeout', 'hl_set_cache_timeout' );

/**
 * Bonus feature: Show custom user registration image (if available) as avatar,
 * otherwise use the default image.
 */
add_filter( 'get_avatar_url', function($url, $user_id, $args) {
    $register_image = get_user_meta($user_id, 'register_image', true);
    return ($register_image) ? $register_image : $url;
} , 10, 3 );
